# jtray

Json TRAY

Simple StatusNotifierWatch implementation for use with bars that doesn't 
implement system tray.

tray entry updates are written to stdout in json format.

Right click menus are handled by IPC on unix socket `{RUNDIR}/jtrayctl`

## Usage

```
jtray 0.1.0
errornonamer <errornonamer@protonmail.ch>
Simple StatusNotifierWatcher implementation for bars that doesn't implement system tray

USAGE:
    jtray [SUBCOMMAND]

OPTIONS:
    -h, --help       Print help information
    -V, --version    Print version information

SUBCOMMANDS:
    get-items        Request to get tray items
    help             Print this message or the help of the given subcommand(s)
    icon-lookup      Lookup icon of a given item and return absolute path to it
    kill             Tell the server to unregister StatusNotifierWatcher, clean up and exit
    ping             Check if server is still alive
    refresh-items    Request to get server to print it's items to the stdout again
    trigger-menu     Send menu activation request
```
