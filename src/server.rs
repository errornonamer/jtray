use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;

use anyhow::{Context, Result};
use clap::Subcommand;
use serde::{Deserialize, Serialize};
use stray::message::menu::TrayMenu;
use stray::message::tray::StatusNotifierItem;
use stray::message::{NotifierItemCommand, NotifierItemMessage};
use stray::StatusNotifierWatcher;
use tokio::io::{AsyncReadExt, AsyncWriteExt, Interest};
use tokio::net::{UnixListener, UnixStream};
use tokio::sync::{broadcast, mpsc, watch, Barrier};

#[derive(Subcommand, Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub enum Commands {
    /// Check if server is still alive
    Ping,
    /// Send menu activation request
    TriggerMenu {
        #[clap(value_parser)]
        submenu_id: i32,
        #[clap(value_parser)]
        notifier_address: String,
    },
    /// Request to get server to print it's items to the stdout again
    RefreshItems,
    /// Request to get tray items
    GetItems,
    /// Lookup icon of a given item and return absolute path to it.
    IconLookup {
        #[clap(value_parser)]
        notifier_address: String,
    },
    /// Tell the server to unregister StatusNotifierWatcher, clean up and exit
    Kill,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub enum Response {
    Acknowledged,
    Data { data: String },
    FailGeneric { cause: Option<String> },
    FailNonexistantItem,
}

impl Response {
    fn from_error(error: &dyn std::error::Error) -> Self {
        Response::FailGeneric {
            cause: Some(error.to_string()),
        }
    }
}

#[derive(Clone, Serialize)]
struct Item {
    entry: StatusNotifierItem,
    menu: Option<TrayMenu>,
}

pub struct Server {
    items: HashMap<String, Item>,
}

impl Server {
    pub fn new() -> Result<Self> {
        eprintln!("Created new server.");

        Ok(Server {
            items: HashMap::new(),
        })
    }

    pub async fn run(&mut self) -> Result<()> {
        let (mcmd_tx, mcmd_rx) = mpsc::channel(32);
        let (icmd_tx, mut icmd_rx) = broadcast::channel(16);
        let (noti_tx, mut noti_rx) = watch::channel(NotifierItemMessage::Remove {
            address: "".to_string(),
        });
        let (cmdr_tx, cmdr_rx) = watch::channel(Response::Acknowledged);
        let barrier = Arc::new(Barrier::new(3));
        eprintln!("Created channels, starting...");
        let notifier_loop = tokio::spawn(Self::notifier_loop(
            StatusNotifierWatcher::new(mcmd_rx).await.unwrap(),
            noti_tx,
            icmd_tx.subscribe(),
            barrier.clone(),
        ));
        let ipc_loop = tokio::spawn(Self::ipc_message_loop(icmd_tx, cmdr_rx, barrier.clone()));

        loop {
            eprintln!("Main: waiting");
            tokio::select! {
                _ = noti_rx.changed() => {
                    eprintln!("Main: got NotifierItemMessage");
                    let message = noti_rx.borrow();
                    eprintln!("got notifier message {:?}", *message);
                    match &*message {
                        NotifierItemMessage::Update { address: id, item, menu } => {
                            self.items.insert(id.to_string(), Item { entry: *item.clone(), menu: menu.clone() } );
                        },
                        NotifierItemMessage::Remove { address: id } => {
                            self.items.remove(id);
                        }
                    }

                    let json = serde_json::to_string(&self.items)?;
                    println!("{}", json);
                },
                message = icmd_rx.recv() => {
                    eprintln!("Main: got Commands");
                    let message = message.context("failed to read cmd")?;
                    match message {
                        Commands::Ping => {
                            eprintln!("Sending result to ipc");
                            cmdr_tx.send(Response::Acknowledged)?;
                        },
                        Commands::Kill => {
                            cmdr_tx.send(Response::Acknowledged)?;
                            barrier.wait().await;
                            notifier_loop.await??;
                            ipc_loop.await??;
                            break;
                        },
                        Commands::TriggerMenu { submenu_id, notifier_address } => {
                            if !self.items.contains_key(&notifier_address) {
                                cmdr_tx.send(Response::FailNonexistantItem)?;
                            }
                            else
                            {
                                let menu_path = &self.items[&notifier_address].entry.menu;
                                if menu_path.is_none() {
                                    cmdr_tx.send(Response::FailGeneric{ cause: Some("That item does not have menu.".to_string()) })?;
                                }
                                else {
                                    let res = mcmd_tx.try_send(NotifierItemCommand::MenuItemClicked {
                                        submenu_id,
                                        menu_path: menu_path.clone().unwrap(),
                                        notifier_address
                                    });
                                    cmdr_tx.send( if res.is_ok() {Response::Acknowledged} else {Response::from_error(&res.err().unwrap())})?;
                                }
                            }
                        },
                        Commands::IconLookup { notifier_address } => {
                            if !self.items.contains_key(&notifier_address) {
                                cmdr_tx.send(Response::FailNonexistantItem)?;
                            }
                            else
                            {
                                let item = &self.items[&notifier_address];
                                if item.entry.icon_name.is_none() {
                                    cmdr_tx.send(Response::FailGeneric{ cause: Some("That item does not have icon set.".to_string()) })?;
                                }
                                else if let Some(p) = &item.entry.icon_theme_path {
                                        let mut lookup = linicon::lookup_icon(item.entry.icon_name.as_ref().unwrap()).with_search_paths(&[p.as_str()])?;
                                        let icon = lookup.next();

                                        if let Some(i) = icon {
                                            cmdr_tx.send(Response::Data{ data: i?.path.into_os_string().into_string().unwrap() })?;
                                        }
                                        else {
                                            // you gotta be kidding me
                                            const FORMAT: [&str; 3] = [".svg", ".png", ".xpm"];
                                            let path = PathBuf::from(p);
                                            for f in FORMAT {
                                                if path.join(format!("{}{}", &item.entry.icon_name.as_ref().unwrap(), f)).as_path().exists() {
                                                    let mut path = p.clone();
                                                    path.push_str(format!("/{}{}", &item.entry.icon_name.as_ref().unwrap(), f).as_str());

                                                    cmdr_tx.send(Response::Data{ data: path })?;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        let mut lookup = linicon::lookup_icon(item.entry.icon_name.as_ref().unwrap()).use_fallback_themes(true);
                                        let icon = lookup.next();
                                        cmdr_tx.send(Response::Data{ data: icon.unwrap()?.path.into_os_string().into_string().unwrap() })?;
                                    }

                            }
                        },
                        Commands::RefreshItems => {
                            println!("{}", serde_json::to_string(&self.items)?);
                            cmdr_tx.send(Response::Acknowledged)?;
                        },
                        Commands::GetItems => {
                            cmdr_tx.send(Response::Data { data: serde_json::to_string(&self.items)? })?;
                        }
                    }
                },
            }
        }

        Ok(())
    }

    async fn notifier_loop(
        watcher: StatusNotifierWatcher,
        tx: watch::Sender<NotifierItemMessage>,
        mut rx: broadcast::Receiver<Commands>,
        barrier: Arc<Barrier>,
    ) -> Result<()> {
        eprintln!("Notifier loop started");
        let mut host = watcher.create_notifier_host("JTrayHost").await?;

        loop {
            eprintln!("Noti: waiting");
            tokio::select! {
                message = host.recv() => {
                    eprintln!("Noti: got NotifierItemMessage");
                    let message = message.context("failed to receive notifier message")?;
                    eprintln!("received notifier message {:?}", message);
                    tx.send(message)?;
                }
                message = rx.recv() => {
                    eprintln!("Noti: got Commands");
                    let message = message.context("failed to read command")?;
                    if message == Commands::Kill {
                        eprintln!("Recived Kill command, escaping notifier loop...");
                        host.destroy().await?;
                        barrier.wait().await;
                        break;
                    }
                },
            }
        }
        Ok(())
    }

    async fn ipc_message_loop(
        tx: broadcast::Sender<Commands>,
        mut rx: watch::Receiver<Response>,
        barrier: Arc<Barrier>,
    ) -> Result<()> {
        eprintln!("IPC loop started");
        let _ = std::fs::remove_file(crate::ipc::Ipc::socket_path());
        let listener = UnixListener::bind(crate::ipc::Ipc::socket_path()).unwrap();
        loop {
            eprintln!("IPC : waiting");
            match listener.accept().await {
                Ok((mut stream, _addr)) => {
                    eprintln!("New connection");
                    let command = Self::parse_ipc_message(&mut stream).await?;
                    Self::handle_command(&mut stream, command.clone(), tx.clone(), rx.clone())
                        .await?;
                    if rx.has_changed()? {
                        eprintln!("HELLO? WTF ARE YOU DOING TOKIO::SYNC::WATCH::RECEIVER??");
                        let _ = rx.borrow_and_update(); // hack: rx.has_changed is set to true here
                                                        // when nothing is sent to it because some
                                                        // async fuckness.. force it to be false
                    }
                    stream.shutdown().await?;

                    if command == Commands::Kill {
                        eprintln!("Received Kill command. escaping ipc loop...");
                        barrier.wait().await;
                        break;
                    }
                }
                Err(e) => {
                    eprintln!("Failed connection: {:?}", e);
                }
            }
        }
        Ok(())
    }

    async fn parse_ipc_message(stream: &mut UnixStream) -> Result<Commands> {
        eprintln!("IPC : parse");
        let _ = stream
            .ready(Interest::READABLE)
            .await
            .context("Failed to ready stream")?;
        let command = Self::read_command(stream).await?;

        eprintln!("message: {:?}", command);

        Ok(command)
    }

    async fn handle_command(
        stream: &mut UnixStream,
        command: Commands,
        tx: broadcast::Sender<Commands>,
        mut rx: watch::Receiver<Response>,
    ) -> Result<()> {
        eprintln!("IPC : handle");
        let _ = stream
            .ready(Interest::WRITABLE)
            .await
            .context("Failed to ready stream")?;

        tx.send(command.clone())
            .context("failed to send command to ipc loop")?;
        rx.changed().await.context("failed to read response")?;
        let response_bytes = bincode::serialize(&*rx.borrow_and_update())?;

        stream
            .try_write(&response_bytes)
            .context("Failed to write response")?;

        stream.shutdown().await?;
        Ok(())
    }

    async fn read_command(stream: &mut UnixStream) -> Result<Commands> {
        let message_len = stream
            .read_u32()
            .await
            .context("Failed to read message length from IPC message")?;
        let mut raw = Vec::<u8>::with_capacity(message_len as usize);
        while raw.len() < message_len as usize {
            stream
                .read_buf(&mut raw)
                .await
                .context("Failed to read message from IPC message")?;
        }

        bincode::deserialize(&raw).context("Failed to parse IPC message")
    }
}
