mod ipc;
mod server;

use std::process::exit;

use anyhow::{Context, Result};
use clap::Parser;
use serde_json::to_string;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(subcommand)]
    command: Option<server::Commands>,
}

fn main() -> Result<()> {
    let cli = Cli::parse();
    if cli.command.is_some() {
        let res = ipc::Ipc::send(cli.command.unwrap())?;
        println!("{}", to_string(&res)?);
        use server::Response::*;
        match res {
            FailGeneric { cause: _ } | FailNonexistantItem => {
                exit(1);
            }
            _ => {}
        }
    } else {
        let mut server = server::Server::new()?;
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .context("Failed to build tokio runtime.")?;
        simple_signal::set_handler(
            &[simple_signal::Signal::Int, simple_signal::Signal::Term],
            move |_| {
                // i'm lazy
                ipc::Ipc::send(server::Commands::Kill).unwrap();
            },
        );
        rt.block_on(server.run())?;
    }
    Ok(())
}
