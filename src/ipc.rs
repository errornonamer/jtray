use std::io::{Read, Write};
use std::os::unix::net::UnixStream;
use std::path::PathBuf;

use anyhow::{Context, Result};

use crate::server::{Commands, Response};

pub struct Ipc;

impl Ipc {
    pub fn socket_path() -> PathBuf {
        std::env::var("XDG_RUNTIME_DIR")
            .map(PathBuf::from)
            .unwrap_or_else(|_| PathBuf::from("/tmp"))
            .join("jtrayctl")
    }

    pub fn send(command: Commands) -> Result<Response> {
        let mut stream = UnixStream::connect(Self::socket_path())?;
        stream
            .set_nonblocking(false)
            .context("Failed to set nonblocking")?;

        let bytes = bincode::serialize(&command)?;
        stream
            .write(&(bytes.len() as u32).to_be_bytes())
            .context("Failed to write message length")?;
        stream
            .write_all(&bytes)
            .context("Failed to write message bytes")?;

        let mut buf = Vec::new();
        stream
            .set_read_timeout(Some(std::time::Duration::from_millis(100)))
            .context("Failed to set read timeout")?;
        stream
            .read_to_end(&mut buf)
            .context("Failed to read response")?;

        stream.shutdown(std::net::Shutdown::Both)?;
        bincode::deserialize(&buf).context("Server sent invalid response")
    }
}
